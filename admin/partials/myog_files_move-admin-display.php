<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://bitbucket.org/myowngames
 * @since      1.0.0
 *
 * @package    Myog_files_move
 * @subpackage Myog_files_move/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
