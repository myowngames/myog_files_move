<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://bitbucket.org/myowngames
 * @since      1.0.0
 *
 * @package    Myog_files_move
 * @subpackage Myog_files_move/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Myog_files_move
 * @subpackage Myog_files_move/admin
 * @author     Paulo Peres Jr <paulo@myog.io>
 */
class Myog_files_move_Admin {
	public static $cron_task_name = 'myog_files_move_cron';
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/myog_files_move-admin.css', array(), $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/myog_files_move-admin.js', array( 'jquery' ), $this->version, false );
	
	}
	
	public static function do_action($action){
		$result_message = '';
		switch ($action) {
			case 'save_config':
				$from = $_POST['from'];
				$to = $_POST['to'];
				$hour = $_POST['hour'];
				if(!empty($from) && !empty($to)){
					update_option('myog_move_files_from', $from);
					update_option('myog_move_files_to', $to);
					update_option('myog_move_files_hour', $hour);
					if(empty($hour)){
						wp_clear_scheduled_hook( self::$cron_task_name );
						$result_message = 'Para ativar o Plugin preencha a hora';
					} else {
						// Se o evento ja existe, dai temos que limpar
						
						if ( wp_next_scheduled( self::$cron_task_name ) ) {
							wp_clear_scheduled_hook( self::$cron_task_name  );
						}
						// pegamos a hora que queresmo rodar
						$hour = (int) $hour;
						$time_local = "$hour:00:00";
						// Fazendo diferença de Offset para rodar na hora certa que foi digitada baseado no Timezone
						$time = strtotime($time_local) + (-1 * (int) ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS ));
						
						wp_schedule_event( $time, 'daily', self::$cron_task_name  );
						
						$result_message = 'Salvo com Sucesso para Rodar no  Horario Local: '.$time_local.' no Horario Servidor: '.date("d-M-Y h:i:sa",$time). ' e o servidor esta com o Horario autal de:'.date("d-M-Y h:i:sa");
						error_log( $result_message );
					}
					
				}else{
					$result_message = "Preencha Corretamente de que pasta e que horario será copiado os arquivos";
				}
				break;
			default:
				break;
		}
		return $result_message;
	}
	public static function edit_settings() {
		if (isset($_REQUEST['action'])) {
			$action = $_REQUEST['action'];
			$result_message = self::do_action($action);
			if ($result_message) {
				?>
					<div class="notice notice-success is-dismissible">
						<p><?php echo $result_message ?></p>
					</div>
				<?php

			}
		}
		?>
			<div class="wrap">
				<h2><?php echo __('Auto Mover Arquivos', 'myog-files_move') ?></h2>
			</div>
			<form method="post">
				<div class="form-wrap">
					<input type="hidden" name="action" value="save_config">
						<table class="form-table">
							<tbody>
								<tr>
									<th scope="row">Mover de Onde </th>
									<td>
										<input type="text" name="from" id="from" value="<?php echo get_option('myog_move_files_from'); ?>" >
										<small>Exemplo: wp-content/themes/pixeus_theme/tabloide_novo/</small>
									</td>
								</tr>
								<tr>
									<th scope="row">Para Onde</th>
									<td>
										<input type="text" name="to" id="to" value="<?php echo get_option('myog_move_files_to'); ?>" >
										<small>Exemplo: wp-content/themes/pixeus_theme/tabloide/</small>
									</td>
								</tr>
								<tr>
									<th scope="row">Que horas? (24 horas)</th>
									<td>
										<input type="text" name="hour" id="hour" value="<?php echo get_option('myog_move_files_hour'); ?>" >
										<small>Exemplo: 08 ou 12 ou 16</small>
									</td>
								</tr>
								
							</tbody>
						</table>

						<div class="form-field">
							<?php echo submit_button("Save"); ?>
						</div>
					</div>
				</form>			
			</form>
		<?php
	}
	public function plugin_action_links($links, $file)
	{	
		if (strpos($file, 'myog_files_move' ) !== false) {
			$config_manager_links = '<a href="/wp-admin/options-general.php?page=myog_move_file_settings" title="Configurar">Configurar</a>';
			array_unshift($links, $config_manager_links);
			
		}

		return $links;
	}
	public function add_to_menu()
	{

		$theme_page = add_submenu_page(
			'options-general.php', 
			__('Auto Mover Arquivos', 'myog-files_move'), 
			__('Auto Mover Arquivos', 'myog-files_move'), 
			'edit_theme_options', 
			'myog_move_file_settings', 
			array('Myog_files_move_Admin', 'edit_settings')
		);
	}


	public function files_move_run_cron() {
		error_log( 'files_move_run_cron' );
		try {
			
			$from = get_option('myog_move_files_from');
			$files = glob("$from*.*");
			error_log( print_r($files, true));
		
			// Não roda se não tem novos arquivos
			if(count($files) > 0){
				$to = get_option('myog_move_files_to');
				$folder =  date("Y-m-d");
				$backup = "$to$folder/";
				// Primeiro move os arquivos do folder para um sub folder
				$this->move_files(
					$to,
					$backup
				);
				// Depois move do antigo pro novo
				$this->move_files(
					$from,
					$to,
					$folder
				);
			}
		} catch (\Throwable $th) {
			throw $th;
			//print_r($th);
			// Se deu errado tenta de novo??
		}
		
		
	}
	private function move_files($from, $to, $append = null) {
		
		
		$path = ABSPATH;
		error_log('move_files',$path);
		$from = "$path$from";
		$to = "$path$to";

		
		// Move all images files
		$files = glob("$from*.*");
		if(count($files) > 0){
			// Create Folder if does not exists
			if (!file_exists($to)) {
				mkdir($to, 0777, true);
			}
		
			foreach($files as $file){
				$file_to_go = str_replace($from,$to,$file);
				if($append){
					$file_name = basename($file);
					$file_to_go = str_replace(
						$file_name,
						$append.$file_name,
						$file_to_go	
					);
				}
				copy($file, $file_to_go);
				unlink($file);
			}
		} else {
			// Not Files to move
			//echo "No Files to move <br>";
		}
	}

	

}
