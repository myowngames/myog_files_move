<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://bitbucket.org/myowngames
 * @since      1.0.0
 *
 * @package    Myog_files_move
 * @subpackage Myog_files_move/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Myog_files_move
 * @subpackage Myog_files_move/includes
 * @author     Paulo Peres Jr <paulo@myog.io>
 */
class Myog_files_move_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		wp_clear_scheduled_hook( 'myog_files_move' );
	}

}
