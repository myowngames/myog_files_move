<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://bitbucket.org/myowngames
 * @since             1.0.0
 * @package           Myog_files_move
 *
 * @wordpress-plugin
 * Plugin Name:       MyOG Files Move
 * Plugin URI:        https://r4t40@bitbucket.org/myowngames/myog_files_move
 * Description:       Move arquivos de uma pasta para outra baseado no tempo, uma vez por dia; Automatizando troca de Tabloides, Para configurar vá em Configurações -> Auto Mover Arquivos
 * Version:           1.0.0
 * Author:            MyOG
 * Author URI:        https://bitbucket.org/myowngames
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       myog_files_move
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'MYOG_FILES_MOVE_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-myog_files_move-activator.php
 */
function activate_myog_files_move() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-myog_files_move-activator.php';
	Myog_files_move_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-myog_files_move-deactivator.php
 */
function deactivate_myog_files_move() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-myog_files_move-deactivator.php';
	Myog_files_move_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_myog_files_move' );
register_deactivation_hook( __FILE__, 'deactivate_myog_files_move' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-myog_files_move.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_myog_files_move() {

	$plugin = new Myog_files_move();
	$plugin->run();

}
run_myog_files_move();
